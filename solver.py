import argparse
from typing import List, Dict
import openpyxl
import itertools
import numpy as np
from table import Table
from glossary import Glossary
from interpret import VariableInterpreter
from idply import Parser
import re


def fill_in_merged(file_name: str, sheet: str = None) -> List[np.array]:
    """
    Loads up a specific sheet and returns it.
    A sheet is comprised of a list of tables.

    :arg file_name: path to the xlsx file.
    :arg sheet: name of the sheet to load in.
    :returns List<np.array>: a list of all the tables in a sheet.
    """
    def rang(m, n): return range(m, n + 1)

    wb = openpyxl.load_workbook(file_name)
    if sheet is not None:
        wb = [wb[sheet]]

    for sheet in wb:
        ranges = list(sheet.merged_cells.ranges)
        for xyrange in ranges:
            b = xyrange.bounds
            sheet.unmerge_cells(str(xyrange))

            for x, y in itertools.product(rang(b[0], b[2]), rang(b[1], b[3])):
                sheet.cell(y, x).value = sheet.cell(xyrange.min_row,
                                                    xyrange.min_col).value
    return [np.array(list(s.values)) for s in wb]


def find_first(sheet: np.array) -> tuple:
    """
    TODO

    :arg sheet: the sheet
    """
    for x, y in itertools.product(*[list(range(s)) for s in sheet.shape]):
        if sheet[x, y]:
            return x, y
    return (None, None)


def explore(sheet: List[np.array], boundaries: List[int]):
    """
    Tries to find the ranges for each table.

    :arg sheet: the sheet containing all the tables.
    :arg bounaries: a list containing the theoretic boundaries.
    :returns None:
    """
    startx = boundaries[0]
    endx = boundaries[2]
    starty = boundaries[1]
    endy = boundaries[3]

    '''horizontal'''
    while True:
        changed = False
        try:
            while any(sheet[endx+1, starty: endy+1]):
                endx += 1
                changed = True
        except IndexError:
            pass
        try:
            while any(sheet[startx: endx+1, endy+1]):
                endy += 1
                changed = True
        except IndexError:
            pass
        if not changed:
            break
    boundaries[2] = endx + 1
    boundaries[3] = endy + 1


def identify_tables(sheets: List[np.array]) -> List[np.array]:
    """
    Function which looks for all the tables in a given sheet.
    Creates a list of boundaries for the tables.

    :arg sheets: a list containing a numpy array representing the sheet.
    :returns List[np.array]: containing boundaries of all the tables.
    """
    tables = []
    for sheet in sheets:
        while True:
            index = find_first(sheet)
            if index[0] is None and index[1] is None:
                break
            boundaries = list(index + index)
            explore(sheet, boundaries)
            tables.append(sheet[boundaries[0]: boundaries[2],
                          boundaries[1]: boundaries[3]].copy())
            sheet[boundaries[0]: boundaries[2],
                  boundaries[1]: boundaries[3]] = None

    return tables


def find_tables_by_name(tables, name):
    """
    Looks for tables based on a regex expression representing their name.

    :arg np.array: the tables.
    :arg str: the name, in regex.
    :returns List<np.array>: the tables found.
    """
    named_tables = []
    for table in tables:
        if re.search(name, table[0, 0], re.IGNORECASE):
            named_tables.append(table)
    return named_tables


def find_glossary(tables: List[np.array]) -> Dict[str, np.array]:
    """
    Locates the glossarytables, and places them in a dictionary for each type.
    The five tables it looks for are:

        * Type
        * Function
        * Constant
        * Relation
        * Boolean

    :returns Dict[str, np.array]: containing the glossary for type, function,
    constant, relation and boolean.
    """
    glossary = {"Type": None, "Function": None, "Constant": None,
                "Relation": None, "Boolean": None}

    glossary["Type"] = find_glossary_table(tables, "type", critical=True)
    glossary["Function"] = find_glossary_table(tables, "function")
    glossary["Constant"] = find_glossary_table(tables, "constant")
    glossary["Relation"] = find_glossary_table(tables, "relation")
    glossary["Boolean"] = find_glossary_table(tables, "boolean")

    return glossary


def find_glossary_table(tables: List[np.array], name: str,
                        critical: bool = False) -> np.array:
    """
    Looks for a specific glossary table with name "name".

    If the critical boolean is set, an error is returned when no glossary is
    found. For example, there should always be a type glossary.
    Non-critical glossaries only print warnings when none are found.

    :arg tables: the list of arrays, each containing a table.
    :arg name: the name of the table to find.
    :arg critical: True if a table needs to be found. E.g. if the table is not
        found, an error is thrown.
    :returns np.array: the glossary table, if found.
    """
    glossaries = find_tables_by_name(tables, name)
    if len(glossaries) == 0:
        if critical:
            raise ValueError("No {} glossary table was found.".format(name))
        else:
            print("INFO: No {} glossary table found.".format(name))
        return None

    if len(glossaries) > 1:
        raise ValueError("Multiple {} glossary tables were"
                         " found.".format(name))
    return glossaries[0]


def find_datatables(tables: List[np.array]) -> np.array:
    """
    Locates the Datatable, which contains data that needs to be expressed in
    the structure or our idp file.

    :arg tables: the list of arrays, each containing a table.
    :returns np.array: containing the datatable
    """
    datatables = find_tables_by_name(tables, r"(?i)DataTable|Data Table")
    return datatables


def find_execute_method(tables: List[np.array]) -> Dict[str, object]:
    """
    Locates the Execute table, which contains the inference method.
    There are four possible inference methods:

        * "get x models", where x is the number of required models;
        * "get all models", in order to get all models;
        * "minimize x", where x is a term to minimize;
        * "maximize x", where x is a term to maximize.

    :arg tables: the list of arrays, each containing a table.
    :returns Dict[str, str]: contains the inference method and its applicable
        variables.
    """
    execute_table = find_tables_by_name(tables, 'Execute')
    inf = {"method": "mx",
           "nbmodels": 1,
           "term": ""}
    if len(execute_table) > 1:
        raise ValueError("Only one execute table allowed")
    # Set the inference method based on the Execute table's content.
    if execute_table:
        cell = str(execute_table[0][1])

        # Check for modelexpansion, minimization and maximization.
        if re.search("(?i) Model*", cell):
            # Extract the amount of models.
            if re.search("all", cell):
                nb = 0
            else:
                nb = re.findall('[0-9]+', cell)[0]
            inf["nbmodels"] = int(nb)
        elif re.search("(?i)Minimize", cell):
            inf["method"] = "min"
            term = re.search("(?i)(?<=Minimize) (.*)\'\]$", cell).groups()[0]
            inf["term"] = term
        elif re.search("(?i)Maximize", cell):
            inf["method"] = "max"
            term = re.search("(?i)(?<=Maximize) (.*)\'\]$", cell).groups()[0]
            inf["term"] = term
    return inf


def find_tables(tables: List[np.array]) -> List[np.array]:
    """
    Looks for decision tables and constraint tables.

    :arg tables: the list of arrays, each containing a table.
    :returns List[np.array]: a list containing only the decision and constraint
        tables.
    """
    tables = list(filter(lambda table:
                         not re.match('Glossary|DataTable|Comment|Data Table',
                                      table[0, 0],
                                      re.IGNORECASE),
                         tables))
    return tables


def create_voc(glossary) -> str:
    """
    Function which creates the vocabulary for the IDP file.

    :returns str: the vocabulary for the IDP file.
    """
    return glossary.to_idp_voc()


def create_main(inf: Dict[str, object], parser: Parser) -> str:
    """
    Function which creates the main for the IDP file.

    :arg inf: a dictionary containing the inference method and it's applicable
        variables (e.g. what the term looks like, or how many models should be
        generated)
    :arg parser: the parser, needed to parse the optimization term.
    :returns str: the main for the IDP file.
    """
    mainstr = "\n\nprocedure main(){\n"
    if inf["method"] == "mx":
        mainstr += "stdoptions.nbmodels = {}\n".format(inf["nbmodels"])
        mainstr += "printmodels(modelexpand(T,S))\n"
    elif inf["method"] == "min" or inf["method"] == "max":
        # Create and add termblock
        # Below line generates something like "term = 1", which is then split.
        # We need to pass 1 as argument otherwise the parser wouldn't recognize
        # the term. It's a dirty workaround but works for now. TODO: fix!
        term = parser.parse(inf["term"], 1, None).split('=')[0]
        termstr = "term t:V{\n"
        termstr += "{}\n".format(term)
        termstr += "}\n\n"

        if inf["method"] == "min":
            mainstr += "printmodels(minimize(T,S,t))\n"
        else:
            mainstr += "printmodels(maximize(T,S,t))\n"
        mainstr = termstr + mainstr
    mainstr += "}\n"
    return mainstr


def create_struct(tables: List[np.array],
                  parser: Parser,
                  glossary: Glossary) -> str:
    """
    The structure consists of two types of data.
    The data supplied by data tables, and the data supplied by DateType and
    DateInt.

    The data inside data tables is interpreted and set as "struct_args".
    "struct_args" is then used in the to_idp_struct() method to form the
    structure.

    :arg tables: the list of arrays, each containing a table.
    :arg parser: the parser to read the headers of data tables.
    :arg glossary:
    :returns str: the structure for the IDP file.
    """

    # Interpret the Datatables.
    glossary.read_datatables(find_datatables(tables), parser)

    # Form the structure.
    struct = "\n\nStructure S:V {\n"
    for pred in glossary.predicates + glossary.types:
        s = pred.to_idp_struct()
        if s is None:
            continue
        struct += s
    struct += "}\n"

    return struct


def create_theory(tables: List[np.array], parser: Parser) -> str:
    """
    Function to create the theory in the IDP format.

    :arg tables: the list of arrays, each containing a table.
    :arg parser: the parser.
    :returns str: the theory for the IDP file.
    """
    theory = "Theory T:V {\n"
    for dt in find_tables(tables):
        t = Table(dt, parser).export()
        if t is None:
            continue
        theory += t
        if __debug__:
            print(t)
    theory += "}\n"
    return theory


def find_auxiliary_variables(tables: List[np.array],
                             parser: Parser) -> List[str]:
    """
    Due to a bug in the IDP systems, the variables in a C# table each need to
    use an auxiliary variable to function properly.
    This has the same name as the variable, but preceded with an underscore.

    This function gets the auxiliary variables for every table.
    """
    aux_var: List[str] = []
    for dt in find_tables(tables):
        aux = Table(dt, parser).find_auxiliary()
        if aux:
            aux_var += aux
    return list(filter(None, aux_var))


def main():
    """
    TODO
    """
    # Parse the arguments.
    argparser = argparse.ArgumentParser(description='Run DMN+ on DMN tables.')
    argparser.add_argument('path_to_file', metavar='path_to_file', type=str,
                           help='the path to the xlsx file')
    argparser.add_argument('name_of_sheet', metavar='name_of_sheet', type=str,
                           help='the name of the sheet to execute')
    argparser.add_argument('-o', '--outputfile', metavar='outputfile',
                           type=str,
                           default=None,
                           help='the name of the outputfile')
    argparser.add_argument('--idp', metavar='idp',
                           type=str,
                           default=None,
                           help='the path to the idp executable')
    args = argparser.parse_args()

    # Open the file on the correct sheet and read all the tablenames.
    filepath = args.path_to_file
    sheetname = args.name_of_sheet
    sheets = fill_in_merged(filepath, sheetname)
    tables = identify_tables(sheets)

    g = Glossary(find_glossary(tables))
    inf = find_execute_method(tables)
    if __debug__:
        print(g)
        print(g.to_idp_voc())

    i = VariableInterpreter(g)
    parser = Parser(i)

    aux_var = find_auxiliary_variables(tables, parser)
    g.add_aux_var(aux_var)

    file_path = None

    struct = create_struct(tables, parser, g)
    voc = create_voc(g)
    theory = create_theory(tables, parser)
    main = create_main(inf, parser)
    if args.outputfile:
        file_path = args.outputfile
        if ".idp" not in args.outputfile:
            file_path += args.name_of_sheet.replace(' ', '_') + ".idp"
        fp = open(file_path, 'w')
        fp.write(voc)
        fp.write(struct)
        fp.write(theory)
        fp.write(main)
        fp.close()

    print('Done parsing!')

    if args.idp:
        idp_path = args.idp
        print("Executing IDP at {}, file {}".format(idp_path, file_path))

        if file_path is None:
            raise ValueError('Can\'t execute IDP without writing to file')

        import os
        os.system("{} -e \"main()\" {} >idptemp.txt 2>idptemp.txt"
                  .format(idp_path, file_path))


if __name__ == "__main__":
    main()

