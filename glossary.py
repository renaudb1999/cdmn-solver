"""
The glossary object contains the entire DMN+ glossary table.
It interprets each line, and creates a Type or Predicate object.

A Type is a "variable" in DMN+. There's three types of Type:

    * Normal Type
    * DateType
    * DateInt

DateType is a representation of dates.
DateType has a different vocabulary and structure.

DateInt is a representation of operation on dates.
DateInt is actually a representation of two things: a DateInt variable, and an
xBetween variable (where x is either Day, Week or Month).
"""

import re
import numpy as np
from typing import List
from idpname import idp_name


class Glossary:
    """
    The Glossary object contains all types, functions and predicates.
    During initialisation, it reads and interprets all the types, functions,
    constants, relations and booleans it can find and it report any errors.
    Once the Glossary is created and initialized without errors, it's
    possible to print out the predicates in their IDP version.
    """
    def __init__(self, glossary_dict: dict):
        """
        Initialise the glossary.
        Create 4 default types, create an empty list of predicates, and
        interpret all 5 different glossaries.

        :arg dict: glossary_dict, the dictionary containing for each glossary
            type their tables.
        """
        self.types = [Type('String', None),
                      Type('Int', None),
                      Type('Float', None),
                      Type('DateString', None)]
        self.predicates: List[Predicate] = []
        self.__read_types(glossary_dict["Type"], 0, 1, 2)
        self.__read_predicates(glossary_dict["Function"], "Function")
        self.__read_predicates(glossary_dict["Constant"], "Constant",
                               zero_arity=True)
        self.__read_predicates(glossary_dict["Relation"], "Relation")
        self.__read_predicates(glossary_dict["Boolean"], "Boolean",
                               zero_arity=True)

    def __str__(self):
        """
        Magic method to convert the Glossary to string.
        Prints out all the types, predicates and functions it contains.
        """
        retstr = "Glossary containing:\n"
        for typ in self.types:
            retstr += "\t{}\n".format(str(typ))
        for pred in self.predicates:
            retstr += "\t{}\n".format(str(pred))
        return retstr

    def contains(self, typestr):
        """
        Checks whether or not a type was already added to the glossary.

        :returns bool: True if the type has been added already.
        """
        for typ in self.types:
            if typestr == typ.name:
                return True
        return False

    def find_type(self, t):
        """
        Looks for types in the glossary.

        :returns List<Type>: the types found.
        """
        return list(filter(lambda x: re.match('^{}$'.format(x.name), t,
                                              re.IGNORECASE), self.types))[0]

    def __read_types(self, array, ix_name=0, ix_type=1, ix_posvals=2):
        """
        Read and interpret all the types listed in the Type glossary.
        When it finds the keyword, it tries to interpret the other columns on
        that row.

        :arg np.array: the numpy array containing the Type glossary.
        :arg int: ix_name, the index for the name column.
        :arg int: ix_type, the index for the type column.
        :arg int: ix_posvals, the type for the posvals column.
        :returns None:
        """
        error_message = ""
        rows, cols = array.shape
        # Skip the first 2 rows, as these are headers.
        for row in array[2:]:
            # Loop over all the rows.
            name = row[ix_name]
            name = name.strip()

            # Get, and try to decypher the type.
            # If we're not able to find the type, raise error.
            typ = row[ix_type]
            try:
                typ = self.find_type(typ)
            except IndexError:
                error_message = ("DataType \"{}\" should be either a"
                                 " (String, Int, Float, DateString) or a"
                                 " user-defined type"
                                 .format(typ))
                raise ValueError(error_message)

            # Check for possible values.
            posvals = row[ix_posvals]
            try:
                if __debug__:
                    print("Possible values:")
                    print(posvals)
                # Match for the int range type, for instance [1, 10].
                match = re.match('(\[|\()(\d+)\s*(?:\.\.|,)\s*(\d+)\s*(\]|\))',
                                 posvals)

            except:  # TODO: find errortype to except and fix except.
                match = None

            # Interpret range of int, if a match was found.
            if match:
                match = list(match.groups())
                if match[0] == '(':
                    match[1] += 1
                if match[-1] == ')':
                    match[2] -= 1
                posvals = '..'.join(match[1:-1])
            if posvals is not None and re.match('^[^\[\]\(\)]+$', posvals):
                posvals = ', '.join([idp_name(x) for x in
                                    re.split('\s*,\s*', posvals)])

            # Check for exception types DateString and DaysBetween and create
            # them if found. Otherwise, create normal Type.
            if typ.name == "DateString":
                self.types.append(DateType(name, typ, posvals))
            elif "Between" in name:
                # DaysBetween needs to be supplied by a list of all the
                # possible dates.
                dates = self._find_dates()
                self.types.append(DateInt(name, typ, posvals, dates))
            else:
                # Create the type and append it to the list.
                self.types.append(Type(name, typ, posvals))

    def __read_predicates(self, array, glosname, ix_name=0,
                          ix_type=1, ix_alias=2, zero_arity=False):
        """
        Method to read and interpret predicates.
        Loops over an array containing only predicates or functions,
        and filters them into subcategories.

        The possible entries are: Relation, Function, partial Function,
            boolean, and relation..

        :arg zero_arity: bool which should be True when the predicate is a
            0-arity predicate (constants and booleans).
        """
        # It's possible that there's no glossary defined.
        if array is None:
            return

        for row in array[2:]:
            full_name = row[ix_name].strip()
            partial = False
            typ = None

            # Check if it's a (partial) function/constant or a
            # relation/boolean.
            if re.match('(partial )?Function|Constant', glosname):
                typ = row[ix_type]
                if typ:
                    typ = typ.strip()

                # Check if it's a partial function
                partial = bool(re.match('(?i)partial', full_name))
                try:
                    typ = self.find_type(typ)
                except TypeError:
                    raise ValueError('DataType of Function "{}" is empty'
                                     .format(full_name))
                except IndexError:
                    raise ValueError('DataType "{}" of "{}" is not an existing'
                                     ' Type'.format(typ, full_name))
                # Find the alias. If none is found, alias is set to None.
                try:
                    alias = row[ix_alias]
                    if alias:
                        alias = alias.strip()
                    if __debug__:
                        print("Found alias: {}".format(alias))
                except IndexError:
                    alias = None

            # The predicate is a relation.
            else:
                # Find the alias. If none is found, alias is set to None.
                try:
                    alias = row[ix_alias-1]
                    if alias:
                        alias = alias.strip()
                    if __debug__:
                        print("Found alias: {}".format(alias))
                except IndexError:
                    alias = None

            # Create the predicate.
            p = Predicate.from_string(full_name, typ, self, alias, partial,
                                      zero_arity)

            # Append the new predicate to the list.
            self.predicates.append(p)

    def lookup(self, string: str):
        """
        TODO
        """
        if __debug__:
            print("Lookup {}:".format(string))
        return list(filter(lambda x: x,
                           map(lambda x: x.lookup(string), self.predicates)))

    def _find_dates(self):
        """
        Lookfs for types containing DateString and returns the list of all the
        dates it holds.
        """
        for datatype in self.types:
            try:
                if datatype.super_type.name == "DateString":
                    return datatype.struct_args
            except AttributeError:
                continue
        raise ValueError("A DateString needs to be declared before a DateInt")

    def read_datatables(self, datatables, parser):
        """
        Reads and interprets the datatables.
        Also checks if the values in the datatables appear in the possible
        values column of the glossary.

        Firstly it checks which columns are input, and which are output.
        A column is an inputcolumn if it contains the table title in the first
        cell.
        Iterates over every outputcolumn, deciphers which predicate the
        outputcolumn represents, and sets it's "struct_args" to a combination
        of the input arguments and the output column's arguments.
        The predicate uses this struct_args to format it's struct string.

        :arg List<np.array>: datatables, containing all the datatables.
        :arg parser:
        :returns None:
        """
        # return
        if len(datatables) == 0:
            return
        for table in datatables:
            inputs = []
            outputs = []
            tablename = table[0][0]
            print(table)
            # First, we find the input and outputcolumns.
            for column in table.T[1:]:
                print(column)
                # If a column contains the table title, it's an inputcolumn.
                if column[0] is not None:
                    # We also want to find out the input variables.
                    inputs.append(column[1:])
                else:
                    outputs.append(column[1:])
            # Secondly, we check if the inputcolumns contain the right values.
            # This also adds those values to the "constructed from" if needed.
            for i, inputarr in enumerate(inputs):
                header = inputarr[0]
                # The header can be just the Type name, or "Type called ..".
                typename = header.split(" ")[0]

                # Look for the type name in the glossary.
                for typ in self.types:
                    if typename == typ.name:
                        typ.check_values(inputarr[1:], tablename)

            # Thirdly, we check if the outputcolumns contain the right values.
            # This also adds those values to the "constructed from" if needed.
            for i, outputarr in enumerate(outputs):
                header = outputarr[0]
                # The header can be a function like "Department of Person".
                # Or it can be a relation. If it's a relation, we continue.
                typename = header.split(" ")[0]
                pred = parser.interpreter.interpret_value(header).pred
                if pred.super_type is None:
                    continue
                typename = pred.super_type.name

                # Look for the type name in the glossary.
                for typ in self.types:
                    if typename == typ.name:
                        typ.check_values(outputarr[1:], tablename)

            # Then we iterate over the outputcolumns.
            for i, output in enumerate(outputs):
                header = output[0]
                # Format the args.
                args = {}
                # Iterate of each row of the outputcolumn.
                # Skip the first cell because it's always None.
                for j, value in enumerate(output[1:]):
                    # Find the inputvals of the same row.
                    inputvals = []
                    for inputcol in inputs:
                        inputval = str(inputcol[j+1])
                        inputvals.append(inputval)
                    inputval = "|".join(inputvals)

                    args[inputval] = value

                header = parser.interpreter.interpret_value(header).pred.name

                # Look for the predicate name.
                success = False
                for pred in self.predicates:
                    if pred.full_name == header or pred.name == header:
                        pred.struct_args = args
                        success = True
                        break
                if not success:
                    raise ValueError("Predicate \"{}\" in datatable but not in"
                                     " glossary".format(header))

    def to_idp_voc(self):
        """
        TODO
        """
        voc = "Vocabulary V {\n"
        voc += ''.join(map(lambda x: x.to_idp_voc(), self.types+self.predicates))
        voc += "}\n"
        return voc

    def add_aux_var(self, aux):
        """
        Some variables need to use auxiliary variables, for instance those
        found in the outputcolumns of C# tables.
        This method allows the creation of those variables.

        :arg List<str> a list containing strings of the variables.
        """
        for var in aux:
            # Split of the predicate name.
            p_name = var.split('(')[0]
            p_name = p_name.replace('_', ' ')
            for p in self.predicates:
                if p_name == p.name:
                    new_name = "_{}".format(p.name)
                    new_p = Predicate(new_name, p.args, p.super_type,
                                      partial=p.partial)
                    self.predicates.append(new_p)


class Type:
    """
    TODO
    """
    def __init__(self, name: str, super_type, posvals="-"):
        """
        :arg str: the name of the type.
        :arg Type: the super type of the type.
        :arg str: posvals, the possible values of the type.
        """
        self.name = name
        self.super_type = super_type
        self.possible_values = posvals

        self.struct_args = []
        self.knows_values = True
        self.source_datatable = ""

        # The most super super type, e.g. an atomic type.
        self.super_super_type = self.find_super_super_type()

        # Check the input.
        if posvals is None:
            raise ValueError("Values column for type {} is empty."
                             " Did you forget a '-'?"
                             .format(self.name))

        # Toggle knows_values if the values are known.
        if posvals == "_" or posvals == "-":
            self.knows_values = False
            self.possible_values = ""

        if re.search("(?i)see_Data_Table|see_DataTable", posvals):
            self.knows_values = False
            self.possible_values = ""
            m = re.search(r"(?i)(?<=see_Data_Table_)(.*?)(?=\Z)"
                          "|(?<=see_DataTable_)(.*?)(?=\Z)"
                          , posvals)
            self.source_datatable = m[0]

    def __str__(self):
        """
        TODO
        """
        return "Type: {}".format(self.name)

    def to_theory(self):
        """
        TODO
        """
        return self.name

    def find_super_super_type(self):
        """
        Finds the highest super type in the chain of super types.

        :returns Type: the supertype
        """
        supert = self.super_type
        if supert is None:
            return None

        basesupers = ["String", "Int", "Float", "DateString"]
        if supert.name not in basesupers:
            supert = supert.super_type
        return supert

    @property
    def basetype(self):
        """
        TODO
        """
        try:
            return self.super_type.basetype
        except AttributeError:
            return self

    def check_values(self, values, tablename):
        """
        Method to check if the values listed in a datatable match with the
        values listed in the possible values column(if a datatable was used).

        If the possible values column is left empty, then it assumes all the
        values are correct and it fills the possible values automatically.
        This is needed so that the type can input these values into constructed
        from.

        If the possible values column contains values, then every value used in
        a datatable needs to match a value in the possible values.

        :returns boolean: True if all the values match.
        :throws ValueError: if a value appears in the datatable but not in
        posvals.
        """

        # We only check the data if the tablename (see datatable ...) is
        # explicitly given or a wildcard (-) was used in the glossary.
        if self.source_datatable not in tablename:
            return

        if self.super_super_type.name == "String" or \
                self.super_type.name == "String":
            # If no possible values were listed, read the datatable values and
            # add them to the possible values.
            if not self.knows_values:
                if self.possible_values is None:
                    self.possible_values = ""

                # Check for each value if it exists already, add it if not.
                for value in values:
                    subvalues = str(value).split(',')
                    for subvalue in subvalues:
                        subvalue = subvalue.strip()
                        subvalue = idp_name(subvalue)
                        regex = r"(?<!\w){}(?!\w)".format(idp_name(subvalue))
                        if not re.search(regex, self.possible_values):
                            if not self.possible_values == "":
                                self.possible_values += ","
                            self.possible_values += " {}".format(subvalue)
                return

            # First check for typos in the datatable.
            for value in values:
                subvalues = str(value).split(',')
                for subvalue in subvalues:
                    subvalue = subvalue.strip()
                    subvalue = idp_name(subvalue)
                    regex = r"(?<!\w){}(?!\w)".format(subvalue)
                    if not re.search(regex, self.possible_values):
                        raise ValueError("Error: value {} in datatable but not"
                                         " in possible values."
                                         .format(subvalue))

        elif self.super_super_type.name == "Int" or \
                self.super_type.name == "Int":
            # For integers we only check if they're in the right range.
            # We don't add them if the possible values is empty.
            if not self.knows_values:
                raise ValueError("The values column for integer type {} was"
                                 " left empty".format(self.name))

            # A range should be declared in the possible values. We need to
            # check if our value is within that range.
            if ".." in self.possible_values:
                leftbound, rightbound = self.possible_values.split("..")
            else:
                leftbound, rightbound = self.possible_values.split(",")
            for value in values:
                subvalues = str(value).split(',')
                for subvalue in subvalues:
                    subvalue = int(subvalue.strip())
                    if int(rightbound) < subvalue or \
                            subvalue < int(leftbound):
                        raise ValueError("Error: value \"{}\" for type {} in"
                                         " datatable but not in"
                                         " range of possible values"
                                         .format(subvalue, self.name))

    def to_idp_voc(self):
        """
        Converts all the information of the Type into a string for the IDP
        vocabulary.

        :returns str: the vocabulary form of the type.
        """
        # Check for 'string', 'int', and other default types which don't need
        # to explicitly be declared.
        if self.name == self.basetype.name:
            return ''

        typename = idp_name(self.name)
        constr_from = 'constructed from' if self.basetype.name == 'String' else '='
        if self.possible_values is None:
            return 'type {}\n'.format(typename)

        voc = ('\ttype {} {} {{ {} }} {}\n'.format(
               typename,
               constr_from,
               self.possible_values,
               '' if self.basetype.name == 'String' else 'isa {}'
                                                         .format(self.basetype
                                                                 .name.lower())
               ))

        return voc

    def to_idp_struct(self):
        """
        Converts all the information of the Type into a string for the IDP
        structure.
        Normal types don't need a structure, as their possible values are
        listed as "constructed from" in the voc.
        This is here for future's sake.

        :returns str: the string for the structure.
        """
        return ""


class DateType(Type):
    """
    TODO
    """
    def __init__(self, name: str, super_type, posvals=None):
        """
        Datestring needs a seperate formatting, because we need to expand the
        dates between start and end date.

        Form the structure arguments first. Date doesn't use "constructed
        from", but instead uses the structure for it's values.
        These are special, because we need to expand them.
        """
        super().__init__(name, super_type, posvals)

        possible_values = self.possible_values.replace("[", "")
        possible_values = possible_values.replace("]", "")
        possible_values = possible_values.replace("“", "")
        possible_values = possible_values.replace("”", "")
        possible_values = possible_values.replace("\"", "")

        if ".." in possible_values:
            dates = possible_values.split("..")
            dates = self.expand_dates(dates[0], dates[1])
            self.struct_args = dates
        elif "," in possible_values:
            dates = possible_values.split(",")
            self.struct_args = dates
        else:
            raise ValueError("Illegal date!")

    def to_idp_voc(self):
        """
        Converts the DateType to a string for the IDP vocabulary.

        :returns str: the string for the vocabulary.
        """
        # Form the voc.
        voc = ('\ttype {} isa string\n'.format(self.name))
        return voc

    def to_idp_struct(self):
        """
        Converts the DateType to a string for the IDP structure.
        This structure contains every possible data.

        :returns str: the string for the vocabulary.
        """

        # Format the struct.
        struct = "\t {} = {{".format(self.name)
        for date in self.struct_args:
            struct += "{}; ".format(date)
        struct += "}\n"
        return struct

    def expand_dates(self, startdate, enddate):
        """
        Generates all the possible dates between a start and end date.
        Uses datetime to check whether days exists (e.g. feb 29).

        :arg str: startdate, the first date.
        :arg str: enddate, the last date.
        :returns List<str>: a list of all the dates, in string form.
        :raises ValueError: if an incorrect date is used, or the startdate is
                            later than the enddate.
        """
        import datetime
        syear, smonth, sday = startdate.split("/")
        eyear, emonth, eday = enddate.split("/")

        try:
            d1 = datetime.datetime(int(syear), int(smonth), int(sday))
            d2 = datetime.datetime(int(eyear), int(emonth), int(eday))
        except ValueError:
            raise ValueError("Incorrect dates used!")
        if d1.date() > d2.date():
            raise ValueError("Startdate later than enddate!")
        dates = ["\"{}/{}/{}\"".format(syear, smonth, sday)]
        while d1.date() != d2.date():
            d1 += datetime.timedelta(days=1)
            date = d1.date()
            dates.append("\"{}/{:02d}/{:02d}\""
                         .format(int(date.year),
                                 int(date.month),
                                 int(date.day)))

        dates.append("\"{}/{}/{}\"".format(eyear, emonth, eday))
        return dates


class DateInt(Type):
    """
    DateInt is a type which represents any type of Date operation.
    These types are: DaysBetween, WeeksBetween and YearsBetween.
    For each two dates, it will calculate their xBetween and format it as a
    relation called "xBetween".
    """
    def __init__(self, name: str, super_type, posvals=None, dates="",
                 datename="Date"):
        """
        TODO
        """
        super().__init__(name, super_type, posvals)
        self.datename = datename
        self.dates = dates
        self.int_name = "{}Int".format(self.name)

    def to_idp_voc(self):
        """
        Formats the DateInt as a string for the IDP vocabulary.
        It consists of two parts.

            * An int which represents the amount of x between.
            * A relation, which lists the amount of x between two dates.

        :returns str: the vocabulary string.
        """
        voc = "\ttype {} isa int\n".format(self.int_name)
        voc += "\t{0}({1}, {2}, {2})\n".format(self.name,
                                               self.int_name,
                                               self.datename)
        return voc

    def to_idp_struct(self):
        """
        Formats the DateInt as a string for the IDP structure.

        DateInt introduces an "xBetween" relation, which lists the amount of
        x between every two dates.
        Comparable to a distance vector between dates.

        :returns str: the vocabulary theory.
        """
        lbound, rbound = self.possible_values.split("..")
        self.struct_args = [lbound, rbound]
        xbetween = self._create_distance_vector_dates(self.dates,
                                                      lbound, rbound)

        struct = "\t{} = {{ {}..{} }}\n".format(self.int_name,
                                                self.struct_args[0],
                                                self.struct_args[1])
        struct += "\t{} = {{".format(self.name)
        for i, pair in enumerate(xbetween):
            struct += "{},\"{}\",\"{}\";".format(pair[2], pair[0], pair[1])
            if i % 7 == 0:
                struct += "\n"
        struct += "}\n"
        return struct

    def to_theory(self):
        """
        Formats the DateInt as a string for the IDP theory.

        :returns str: the theory form of the xBetween.
        """
        return self.int_name

    def _create_distance_vector_dates(self, dates, lbound, rbound):
        """
        Method to calculate for each pair of dates the amount of x in
        between them.
        This x can be: days, or weeks.

        :arg List<str>: the list of all the dates.
        :arg int: lbound, the amount of x we want to look in the past.
        :arg int: rbound, the amount of x we want to look in the future.

        :returns List<List<str, str, int>>: the distance vector.
        """
        import datetime
        # Find out which x function is needed:
        if "Days" in self.name:
            xfunction = self._days_between
        elif "Weeks" in self.name:
            xfunction = self._weeks_between
        elif "Years" in self.name:
            xfunction = self._years_between
        else:
            raise ValueError("Date operator not yet implemented: {}"
                             .format(self.name))
        daysbetween = []
        lbound = int(lbound)
        rbound = int(rbound)
        for date1 in dates:
            # Create first datetime object.
            date1 = date1.replace("\"", "")
            syear, smonth, sday = date1.split("/")
            d1 = datetime.datetime(int(syear), int(smonth), int(sday))

            for date2 in dates:
                # Create second datetime object.
                date2 = date2.replace("\"", "")
                eyear, emonth, eday = date2.split("/")
                d2 = datetime.datetime(int(eyear), int(emonth), int(eday))
                x_amount = xfunction(d1, d2)
                # Don't save the difference if it's outside the bounds.
                if rbound < x_amount or x_amount < lbound:
                    continue
                delta = [date1, date2, abs(x_amount)]
                daysbetween.append(delta)
        return daysbetween

    def _days_between(self, datetime1, datetime2):
        """
        Finds the amount of days between two datetimes.

        :arg datetime:
        :arg datetime:
        :returns int: the days between the two datetimes.
        """
        return abs((datetime1-datetime2).days)

    def _weeks_between(self, datetime1, datetime2):
        """
        Finds the amount of weeks between two datetimes.

        :arg datetime:
        :arg datetime:
        :returns int: the weeks between the two datetimes.
        """
        import datetime
        # https://stackoverflow.com/questions/14191832/how-to-calculate-difference-between-two-dates-in-weeks-in-python
        monday1 = (datetime1 - datetime.timedelta(days=datetime1.weekday()))
        monday2 = (datetime2 - datetime.timedelta(days=datetime2.weekday()))

        return (monday2 - monday1).days // 7

    def _years_between(self, datetime1, datetime2):
        """
        Finds the amount of years between two datetimes.

        :arg datetime:
        :arg datetime:
        :returns int: the years between the two datetimes.
        """
        from dateutil.relativedelta import relativedelta
        return relativedelta(datetime1, datetime2).years


class Predicate:
    """
    TODO
    """
    def __init__(self, name: str, args: List[Type], super_type: Type,
                 alias=None, partial=False, full_name=None, zero_arity=False):
        """
        Initialises a predicate.
        :arg zero_arity: bool which should be True when the predicate is a
            0-arity predicate (constants and booleans).
        """
        self.name = name
        self.args = args
        self.super_type = super_type
        self.partial = partial
        self.repr = self.interpret_alias(alias)
        self.full_name = full_name
        self.struct_args = {}
        if __debug__:
            print("Created Predicate {}".format(self.name))
            print(self.args)

        if not self.args and self.super_type and not zero_arity:
            print('WARNING: "{}" has been interpreted as single value'
                  ' instead of a function. Functions should be defined'
                  ' as FunctionName of Type and Type ...'
                  .format(self.name))
        elif not self.args and not self.super_type and not zero_arity:
            print('WARNING: "{}" has been interpreted as a boolean value'
                  ' instead of a relation. Relations should be defined'
                  ' as Type and Type ... is RelationName'
                  .format(self.name))

    def __str__(self):
        """
        TODO
        """
        retstr = "Predicate: {}".format(self.name)
        return retstr

    @staticmethod
    def from_string(full_name: str, super_type: Type, glossary: Glossary,
                    alias=None, partial=False, zero_arity=False):
        """
        Static method to create a predicate from string.

        :arg str: full_name, the full name.
        :arg Type: super_type, the super type of the predicate.
        :arg Glossary: glossary, the glossary.
        :arg str: alias, whether or not there's an alias.
        :arg bool: partial, whether or not it's a partial function.
        :arg zero_arity: bool which should be True when the predicate is a
            0-arity predicate (constants and booleans).
        :returns Predicate:
        """
        if super_type:  # Check if it's a function.
            regex = ("^(?P<name>.*) of (?P<args>(?:{0})(?: and (?:{0}))*)$"
                     .format('|'.join([x.name for x in glossary.types])))
        else:
            regex = ('^(?P<name>.*)$')
        #    regex = ('^(?P<args>(?:{0})(?: and (?:{0}))*) is (?P<name>.*)$'
        #             .format('|'.join([x.name for x in glossary.types])))
        try:
            name = re.match(regex, full_name).group('name')
        except AttributeError:
            name = full_name
        try:
            args = re.match(regex, full_name).group('args').split(' and ')
        except (AttributeError, IndexError):
            if zero_arity:
                return Predicate(full_name, [], super_type, alias, partial,
                                 zero_arity=zero_arity)
            else:  # We need to find the relation's types.
                args = []
                name_elements = full_name.split(" ")
                for t in glossary.types:
                    for element in name_elements:
                        if re.match(element, t.name):
                            args.append(t)
                            print(t.name, element)
                return Predicate(name, args,
                                 super_type, alias, partial, full_name, zero_arity)

        return Predicate(name, [glossary.find_type(t) for t in args],
                         super_type, alias, partial, full_name, zero_arity)

    def interpret_alias(self, alias):
        """
        Method to interpret an alias.
        This method forms a generic name representation, by replacing the
        arguments by dummies.
        In this way, it creates a skeleton structure for the name.

        Thus, it returns the name, without the arguments.
        For instance, `Country border Country` becomes
        `(?P<arg0>.+) borders (?P<arg1>.+)`.
        This way, arg0 and arg1 can be found easily later on.
        """
        if alias is None and not self.args:
            return self.name
        elif alias is None and self.args and not self.super_type:
            name_elements = self.name.split(" ")
            new_alias = ""
            arg_index = 0
            arglist = [arg.name for arg in self.args]
            for element in name_elements:
                if element in arglist:
                    new_alias += "(?P<arg{}>.+) ".format(arg_index)
                    arg_index += 1
                    continue
                else:
                    new_alias += "{} ".format(element)
            return new_alias[:-1]  # We drop the last space.
        elif alias is None and self.args and self.super_type:
            return '^{0} of {1}$'.format(self.name, ' and '.join(
                "(?P<arg{}>.+)".format(i) for i, _ in enumerate(self.args)))
        elif alias is not None:
            if re.match('({0})\s*({0})'
                        .format('|'.join([x.name for x in self.args])), alias):
                raise ValueError('alias "{}" should have non-space'
                                 ' characters seperating types'.format(alias))
            for i, arg in enumerate(self.args):
                if arg.name not in alias:
                    raise ValueError('alias "{}" does not contain'
                                     ' argument "{}"'.format(alias, arg.name))
                alias = alias.replace(arg.name, '(?P<arg{}>.+)'.format(i), 1)
            return alias
        else:
            raise ValueError("No idea what went wrong.")

    def lookup(self, string: str):
        """
        TODO
        """
        d = re.match(self.repr, string)
        # print("Repr:", self.repr, d)
        if d:
            d = d.groupdict()
            return self, [v for k, v in sorted(d.items(),
                                               key=(lambda x: int(x[0][3:])))]

    def to_idp_voc(self):
        """
        Convert the predicate/function to a string for the IDP vocabulary.

        :returns str: the predicate/function in vocabulary format.
        """
        voc = '\tpartial ' if self.partial else '\t'
        voc += '{}'.format(idp_name(self.name))
        if self.args:
            voc += '({})'.format(', '.join(map(lambda t: idp_name(t.name),
                                               self.args)))
        if self.super_type is not None:
            voc += ': {}'.format(idp_name(self.super_type.name))
        return voc + "\n"

    def to_idp_struct(self):
        """
        If a function or predicate receives a value in a datatable, we need to
        set it's values in the structure.
        When parsing the datatable in "read_datatables", we set the
        "struct_args" of the predicates/functions that get a value.
        struct_args could look like: {key1|key2:value}. However, it's possible
        to input multiple keys per cell to save space.

        For instance:
        "Jim|Skydiving, Soccer" needs to be formatted as
        "Jim, Skydiving; Jim, Soccer".
        The same goes for functions.

        To achieve this, we split the keys on their seperator, and then we
        split each key on a comma. This way, we have an array of keys in which
        each item is an array of subkeys. We need to form every possible
        combination of these keys, and to do this we use itertools.product.
        """
        import itertools
        if len(self.struct_args) == 0:
            return None
        struct = '\t{} = {{'.format(idp_name(self.name))
        # If the pred is a function, the format is "arg,.. -> arg".
        if self.super_type:
            for key, arg in self.struct_args.items():
                keys = key.split('|')
                keys = [x.split(',') for x in keys]
                keys_product = itertools.product(*keys)
                for combination in list(keys_product):
                    idp_combination = [idp_name(x.strip()) for x in combination]
                    struct += "{} -> {};".format(','.join(idp_combination),
                                                 idp_name(arg))
            struct += '}\n'

        else:
            for key, arg in self.struct_args.items():
                # Check if the relation is a boolean (booleans have no keys).
                if key == "":
                    if re.match("(?i)yes", arg):
                        struct = "\t{} = true\n".format(idp_name(self.name))
                        return struct
                    if re.match("(?i)no", arg):
                        struct = "\t{} = false\n".format(idp_name(self.name))
                        return struct
                # Only add a relation if the value of the argument is yes.
                if not re.match("(?i)yes", arg):
                    continue
                    print(key, arg)
                # The key can consist of multiple values.
                keys = key.split('|')
                keys = [x.split(',') for x in keys]
                keys_product = itertools.product(*keys)
                for combination in list(keys_product):
                    idp_combination = [idp_name(x.strip()) for x in combination]
                    struct += ",".join(idp_combination) + ";"
            struct += '}\n'

        return struct
