
sheets=("Map_Coloring" "Monkey_Business" "Reindeer_Ordering" "Hamburger_Challenge" "Balanced_Assignment"
        "Duplicate_Product_Lines" "Customer_Greeting" "Crack_the_Code"
        "Family_Riddle" "Map_Coloring_Violations" "Zoo_Buses_and_Kids" "Online_Dating" "Collection_of_Cars"
        "Who_Killed_Agatha" "Vacation_Days" "Vacation_Days_Advanced" "Change_Making" "EquationHaiku_alt"
        "Nim_Rules")
data="Examples/DMChallenges.xlsx"
tempfile="temp.txt"
idpfile="idptemp.txt"
idppath="~/idp/usr/local/bin/idp"

for sheet in ${sheets[*]};
do
    echo $data "$sheet"
    python3 -O solver.py $data "$sheet" -o "./temp.idp"> $tempfile 2> $tempfile --idp $idppath
    if grep -q "Error" $tempfile; then
        echo "Probleem!"
        exit 1
    fi
    echo ".idp file made!"
    if grep -q "Unsatisfiable"  $idpfile; then
        echo "Unsatisfiable!"
        exit 1
    fi
    echo "Satisfiable!"
done
