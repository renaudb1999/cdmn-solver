def idp_name(string: str):
    """
    Formats a string to be compatible for the IDP system.
    This means that it changes some characters to others.

    :arg string: the string to change.
    :returns str: the IDP compatible string.
    """
    return '{}'.format(string)\
        .replace(' ', '_')\
        .replace('-', '_')\
        .replace('“', '"')\
        .replace('”', '"')
