import numpy as np
import re
from idply import Parser
from idpname import idp_name
from typing import List, Dict


def variname(string: str) -> str:
    """
    Function to return the variable of a header in the form of "Type called
    var"

    :arg string: the headerstring of which the variable name needs to be found.
    :returns str: the variable name.
    """
    return re.split('\s+[cC]alled\s+', string)[-1]


class Table:
    """
    The table object represents decision and constraint tables.

    :attr name: str
    :attr hit_policy: str
    :attr inputs: List[np.array]
    :attr outputs: List[np.array]
    :attr rules: List[np.array]
    """
    def __init__(self, array: np.array, parser: Parser):
        """
        Initialises a table object for decision or constraint tables.
        Table interprets and splits up every table into inputs, outputs, rules,
        name and hit policy, after which it doesn't save the table array.

        :arg array: the np.array containing the table.
        :arg Parser: the parser
        :returns Table:
        """
        self.inputs: List[np.array] = []
        self.outputs: List[np.array] = []
        self.rules: List[np.array] = []
        self.name = self._read_name(array)
        self.hit_policy = self._read_hitpolicy(array)

        self._read_inputs(array)
        self._read_outputs(array)
        self._read_rules(array)
        self.parser = parser

    def _read_name(self, array: np.array) -> str:
        """
        Method to read the name of a table, which is located in the top-left
        cell.

        :arg array: the np.array containing the table.
        :returns str: the name of the table.
        """
        return array[0, 0]

    def _read_hitpolicy(self, array: np.array) -> str:
        """
        Method to read the hit policy of a table, which is located at [1,0].

        :arg array: the np.array containing the table.
        :returns str: the hit policy of the table.
        """
        # Single cells containing value shouldn't be interpreted as tables.
        if len(array) == 1:
            return "None"  # None in string, because it's used in regex checks.
        return array[1, 0]

    def _read_inputs(self, array: np.array) -> None:
        """
        Method to read all the input columns of a table.
        A column is an input column if the first cell contains the table name.
        E.g. the columns under the merged cells representing the name.

        :arg array: the np.array containing the table.
        :returns None:
        """
        for x in range(1, array.shape[1]):
            if array[0, x] != self.name:
                return
            self.inputs.append(array[1, x])

    def _read_outputs(self, array: np.array) -> None:
        """
        Method to read all the output columns of a table.
        A column is an output column if the first cell doesn't contain the
        table name.
        E.g. all columns not under the merged cells representing the name.

        :arg array: the np.array containing the table.
        :returns None:
        """
        for x in range(1, array.shape[1]):
            if array[0, x] == self.name:
                continue
            self.outputs.append(array[1, x])

    def _read_rules(self, array: np.array) -> None:
        """
        Method to read all the rules of a table.
        A rule is basically all the rows of a table.

        :arg array: the np.array containing the table.
        :returns None:
        """
        # Single cells containing value shouldn't be interpreted as tables.
        if len(array) == 1:
            return
        for x in range(1, array.shape[0]):
            if array[x, 0] != self.hit_policy:
                break
        self.rules = array[x:, 1:]

    def _read_types(self, array: np.array, filter_args=None):
        """
        Method to interpret the headers in a table.
        Interprets "Foo called Bar", and returns Bar.

        :args array: the np.array containing the table.
        :returns str: the variablename.
        """
        # Interpret the headers in a table.
        for header in array:
            tvals = re.split('\s+[cC]alled\s+', header)
            try:
                t = self.parser.interpreter.glossary.find_type(tvals[0]
                                                               .strip())
            except IndexError:
                if len(tvals) == 2:
                    raise ValueError('{} is not a type, but is used with'
                                     ' \"called\" keyword'.format(tvals[0]))
                else:
                    continue
            if filter_args is not None and not filter_args(tvals[-1]):
                continue
            yield tvals[-1], t

    def _create_quantors(self,
                         array: np.array,
                         filter_args=None,
                         repres='!{0}[{1}]: ') -> str:
        """
        Creates the quantors using a specific format.

        :arg array: the np.array containing the table.
        :arg filter_args:
        :arg repres: a string containing a Pythonic string format.
        :returns str: quantorstr
        """
        return ''.join(repres.format(idp_name(x[0]),
                                     idp_name(x[-1].to_theory())) for x in
                       self._read_types(array, filter_args))

    def _create_quantors_list(self,
                              array: np.array,
                              filter_args=None) -> List[str]:
        """
        Creates a list of all possible quantors for a row.

        :returns List[str]: a list containing all the quantors.
        """
        repres = '{0}[{1}]'
        return [repres.format(x[0], x[-1].to_theory()) for x in
                self._read_types(array, filter_args)]

    def _clear_double_quantors(self, quantorstr: str, quantorlist: List[str]):
        """
        Looks for elements of a list inside a string and removes them.
        Is used to make sure no variable is quantified twice in an aggregate.
        For instance: "!x[object]: Total(x) = #{x[object]: x}." has a double
        quantor for object x, of which the one in the quantor needs to be
        removed.

        :arg quantorstr: the string which needs to be stripped of double
            quantors.
        :arg quantorlist: the list containing the quantors that are already
            present outside the aggregate.
        :returns str: the new quantorstring.
        """
        for quantor in quantorlist:
            quantorstr = quantorstr.replace(quantor, '')
        return quantorstr

    @property
    def fstring(self) -> str:
        """
        Method to decide the string format for a certain hit policy.

        :returns str: the format string.
        """
        if self.hit_policy == 'E*':
            return '\t{0}{1} => {2}{3}.\n'
        elif re.match('[UA]', self.hit_policy):
            return '\t\t{0}{2}{3} <- {1}.\n'
        return ""

    def _export_definitions(self) -> str:
        """
        Method to export the table as definitions.
        When the hitpolicy is 'U', or 'A', we can translate the entire table
        into definitions in idp form.

        :returns str: the table as definitions.
        """
        # Set the headername in comments.
        string = '\t//{}\n'.format(self.name)

        # Iterate over every outputcolumn.
        for i, col in enumerate(self.outputs):
            string += '\t{\n'
            iquantors = self._create_quantors(self.inputs)
            oquantors = self._create_quantors(self.outputs)
            variables = dict(self._read_types(self.inputs + self.outputs))
            # Iterate over every row and interpret it for the specific
            # outputcolumn (and disregard the other outputcolumns).
            falsecount = 0
            for row in self.rules:
                conditions = ' & '.join(filter(
                    lambda x: x, (self.parser.parse(variname(col), row[i],
                                  variables) for i, col in
                                  enumerate(self.inputs))))
                conclusion = self.parser.parse(col, row[i + len(self.inputs)],
                                               variables)
                # A definition can't contain a not in the conclusion.
                # A negation of a predicate is implied by the other rules.
                if '~' in conclusion:
                    # If all the row are 'not', we need to specify that none of
                    # the predicates are true because there's no implicit
                    # rule which defines this.
                    if falsecount == len(self.rules) - 1:
                        conditions = "false"
                        conclusion = conclusion.replace("~", " ")
                        conclusion = conclusion[2:-1]  # Strip the brackets.
                    else:
                        falsecount = falsecount + 1
                        continue
                    if __debug__:
                        print("Skipping rule \"{}\" to avoid having a negation"
                              " in a definition".format(conclusion))
                if '<=' in conditions:
                    conditions = conditions.replace('<=', '=<')
                if not conclusion:
                    continue
                if not conditions:
                    conditions = 'true'
                string += '\t\t{0}{2}{3} <- {1}.\n'.format(iquantors,
                                                           conditions,
                                                           oquantors,
                                                           conclusion)
            string += '\t}\n\n'
        return string

    def _export_implication(self) -> str:
        """
        Method to export the table as implications.
        When the hitpolicy is 'E*', we can translate the entire table into
        implications in idp form.

        :returns str:
        """
        # Set the headername in comments.
        string = '\t//{}\n'.format(self.name)
        # Depending on the inputs and outputs, we need different input and
        # output quantors.
        iquantors = self._create_quantors(self.inputs)
        oquantors = self._create_quantors(self.outputs)
        variables = dict(self._read_types(self.inputs + self.outputs))

        # For each row, form the conditions and the conclusions.
        # When no conditions are present, the condition defaults to 'true'.
        for row in self.rules:
            conditions = ' & '.join(filter(
                lambda x: x, (self.parser.parse(variname(col), row[i],
                                                variables) for i, col
                                    in enumerate(self.inputs))))
            conclusions = ' & '.join(filter(
                lambda x: x, (self.parser.parse(col, row[i + len(self.inputs)],
                                                variables) for i, col 
                              in enumerate(self.outputs))))
            if not conclusions:
                raise ValueError('This line has no conclusion: {}'.format(row))
            # <= is inverted in idp.
            if '<=' in conclusions:
                conclusions = conclusions.replace('<=', '=<')
            if not conditions:
                conditions = 'true'
            string += '\t{0}{1} => {2}{3}.\n\n'.format(iquantors, conditions,
                                                       oquantors, conclusions)
        return string

    def _export_aggregate(self) -> str:
        """
        Method to export the table as aggregates.
        When the hitpolicy is 'C+/>/</#', we can translate the table into
        aggregates in idp form.

        An aggregate is of the form: "aggr{ variables : condition : weights}"
        or in the case of count:     "#{variables : condition}".

        :returns str:
        """

        # Conversion table from specific hitpolicy to aggregate.
        conversion = {
            'C+': 'sum',
            'C>': 'max',
            'C<': 'min',
            'C#': '#'
        }
        # Set the headername in comments.
        string = '\t//{}\n'.format(self.name)

        for i, col in enumerate(self.outputs):

            try:
                args = [x for x in
                        self.parser.interpreter.interpret_value(col).args]
            except AttributeError:
                raise ValueError("Column in table {} in list of predicates."
                                 "Maybe you forgot the merge all the inputs?"
                                 .format(self.name))
            additional_inputs = [x.pred.super_type.name
                                 for x in args if hasattr(x, 'pred')]

            # Create the iquantors, for instance !type[Type]
            # Later on we need to make sure the aggregates don't contain the
            # same quantor, to avoid issues. We use iquantor list for this
            # purpose.
            iquantors = self._create_quantors(self.inputs + additional_inputs,
                                              lambda x:
                                              x in [x.value for x in args])
            iquantor_list = self._create_quantors_list(self.inputs +
                                                       additional_inputs,
                                                       lambda x:
                                                       x in [x.value for x in
                                                             args])

            # Create the assigned variable, for instance Function(Type).
            assigned_variable = self.parser.interpreter.interpret_value(col)
            assigned_variable = '{}({})'.format(
                idp_name(assigned_variable.pred.name),
                ', '.join([arg.value for arg in assigned_variable.args]))

            variables = dict(self._read_types(self.inputs + self.outputs))

            if list(self._read_types(self.outputs)):
                raise NotImplementedError('quantors in output columns'
                                          ' are not yet supported')

            # Create a formatstring "fstring", by setting the correct
            # aggregate.
            # Generates a string like "sum{{ {0}: {1}: {2} }}".
            if self.hit_policy == 'C#':
                fstring = '{0}{{{{ {{0}}: {{1}} & {{2}} }}}}'.format(
                        conversion[self.hit_policy])
            else:
                fstring = '{0}{{{{ {{0}}: {{1}}: {{2}} }}}}'.format(
                        conversion[self.hit_policy])

            # Create the variables for the aggregate.
            agg_variables = self._create_quantors(self.inputs,
                                                  lambda x: x not in args,
                                                  repres='{0}[{1}] ')
            agg_variables = self._clear_double_quantors(agg_variables,
                                                        iquantor_list)

            aggs = []
            # Every row now gets formatted according to the formatstring.
            for row in self.rules:

                # Iterate over each column of the row and interpret the values.
                # Cells which can't be parsed return None values, we need to
                # filter these.
                parsed_cells = [self.parser.parse(variname(col),
                                                  row[i],
                                                  variables) for
                                i, col in enumerate(self.inputs)]
                conditions = list(filter(lambda x: x, parsed_cells))
                extra_conditions = ['{} = {}'.format(arg, arg.value) for arg in
                                    args]

                # Add the conditions into one string, seperated by '&'.
                conditions = ' & '.join(conditions + extra_conditions)

                # If no conditions are specified, default to 'true'.
                if not conditions:
                    conditions = 'true'

                # Get the weights of the aggregate.
                weights = self.parser.parse('__PLACEHOLDER__',
                                            row[i + len(self.inputs)],
                                            variables)
                if not weights or not re.search('__PLACEHOLDER__\s*=',
                                                weights):
                    raise ValueError(
                        'There is no valid value apointed to {}\'s weights in'
                        ' the following rule: {}'
                        ' Maybe you forgot to merge the correct cells?'
                        .format(col, row))
                if self.hit_policy == 'C#':
                    try:
                        x = self.parser.interpreter.interpret_value(
                                row[i + len(self.inputs)])
                        additional_agg_vars = self._create_quantors(
                            [x.pred.super_type.name],
                            lambda x: x not in args, repres='{1}[{0}] ')
                        additional_agg_vars = self._clear_double_quantors(
                                                  additional_agg_vars,
                                                  iquantor_list)
                        weights = re.sub('__PLACEHOLDER__', x.value,
                                         weights)
                    except AttributeError:
                        additional_agg_vars = ''
                        weights = 'true'
                    except TypeError:
                        raise TypeError("Failed looking up {} at row {} of"
                                        " table '{}'"
                                        .format(row[i + len(self.inputs)],
                                                i + len(self.inputs),
                                                self.name))
                else:
                    weights = re.sub('__PLACEHOLDER__\s*=\s*', '',
                                     weights)
                    additional_agg_vars = ''

                aggs.append(fstring.format(agg_variables + additional_agg_vars,
                                           conditions, weights))

            if self.hit_policy == 'C#':
                string += ('\t{}{}_{} = {}({}).\n'
                           .format(iquantors,
                                   " " if iquantors else "",
                                   assigned_variable,
                                   "sum",
                                   ','.join(aggs)))
                string += ('\t{{\n\t\t{0}{1} = _{1}.\n\t}}\n'
                           .format(iquantors, assigned_variable))
            else:
                string += ('\t{}{}{} = {}({}).\n'
                           .format(iquantors,
                                   assigned_variable,
                                   " " if iquantors else "",
                                   conversion[self.hit_policy],
                                   ','.join(aggs)))
        return string

    def export(self):
        """
        Export tries to find the hit policy for a table, and then returns the
        method needed to transfer the table to idp form.
        These hit policies are currently:

          * A, U  -> translate to definitions;
          * E*    -> translate to implications;
          * C+    -> translate to aggregates.

        Every hit policy has it's own method.

        :returns method: the export method for the table.
        """

        # List all possible hit policies.
        actions = {
            '^[AU]$': self._export_definitions,
            '^E\*$': self._export_implication,
            '^C[\<\>\#\+]$': self._export_aggregate
        }
        # Try, except is necessary to avoid StopIteration error.
        try:
            hp = next(map(lambda x: x.re.pattern, filter(lambda x: x, (re.match(x, self.hit_policy) for x in actions))))
        except StopIteration:
            return None
        return actions[hp]()

    def find_auxiliary(self) -> List[str]:
        """
        Every output in a C# table needs to use an auxiliary variable to work
        correctly.
        This method makes a list of those output variables, so that the
        auxiliary versions can be created.

        :returns List[str]:
        """
        if "C#" != self.hit_policy:
            return None

        aux_var = []
        for i, col in enumerate(self.outputs):
            assigned_variable = self.parser.interpreter.interpret_value(col)
            assigned_variable = '{}'.format(
                idp_name(assigned_variable.pred.name))
            assigned_variable = assigned_variable.replace('_', ' ')
            aux_var.append(assigned_variable)
        return aux_var
