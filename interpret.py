import re

from glossary import Glossary, Predicate
from idpname import idp_name
from typing import List, Dict


class VariableInterpreter:
    """
    TODO
    """
    def __init__(self, glossary: Glossary):
        """
        Initialises the VariableInterpreter.

        :arg glossary: a glossary object.
        :returns Object:
        """
        if __debug__:
            print("Created Interpreter")
        self.glossary = glossary

    def interpret_value(self,
                        value: str,
                        variables: List = [],
                        expected_type=None) -> str:
        """
        Method to interpret a value of a DMN notation.

        :arg value: string which needs to be interpreted.
        :arg variables: list containing the variables.
        :arg expected_type: 
        :returns str: the interpretation of the notation.
        """
        # TODO: instead of string variables, make Variable class with existing
        # type to which can be referred.
        lu = self.glossary.lookup(str(value))
        interpretations = []
        # print(lu)
        # breakpoint()

        if len(lu) == 0:
            try:
                if __debug__:
                    print("Expected " + str(expected_type))
            except IndexError:
                pass
            except KeyError:  # TODO: Dit weghalen
                pass
            return Value(value, expected_type, variables)
        for l in lu:
            if expected_type is not None and expected_type != l[0].super_type:
                if __debug__:
                    print("Expected type is not none and expected_type != sup")
                    print(expected_type, l[0].super_type)
                # continue
                pass  # TODO: fix this!
            try:
                interpretations.append(
                        PredicateInterpretation(l[0], l[1], self, variables))
            except ValueError:
                continue
        if len(interpretations) == 0:
            raise ValueError('The value of "{}" could not be interpreted.'
                             .format(value))
        if len(interpretations) == 1:
            return interpretations[0]
        if len(interpretations) > 1:
            print("Warning: Multiple possible interpretations for {}."
                  " Selecting the last one found."
                  .format(value))
            return interpretations[-1]


class Value:
    """
    An object to represent a value.
    """
    def __init__(self, value: str, valuetype, variables):
        """
        Initialised a Value object.

        :arg value: a string containing the value.
        :arg valuetype:
        :arg variables:
        :returns Object:
        """
        self.value = value
        self.type = valuetype
        self.check(variables)

    def check(self, variables):
        """
        TODO
        """
        if re.match('.* (and|of) .*', str(self.value)):
            raise ValueError('The compiler does not know how to interpret'
                             ' the following: "{}".'.format(self.value))
        return
        # TODO: reinclude below code.
        if self.value not in variables.keys() and \
                self.value not in self.type.possible_values:
            raise ValueError('WARNING: {} occurs in a position of type {}'
                             ' but does not appear in possible values'
                             .format(self.value, self.type.name))

    def __str__(self) -> str:
        """
        Magic method to format a variable interpretation to string.

        :returns str:
        """
        return '{}'.format(idp_name(self.value))


class PredicateInterpretation:
    """
    TODO
    """
    def __init__(self, pred: Predicate,
                 arguments,
                 inter: VariableInterpreter,
                 variables):
        """
        Initialises the PredicateInterpretation object.

        :arg pred: the predicate to interpret.
        :arg arguments:
        :arg inter: the variable interpreter.
        :arg variables:
        :returns Object:
        """
        if __debug__:
            print("Creating PredicateInterpretation of:")
            print("\t", pred, arguments)
        self.pred = pred
        self.args = [inter.interpret_value(arg, variables=variables,
                                           expected_type=t) for arg, t in
                     zip(arguments, pred.args)]
        # if

    def type(self):
        """
        Method to get the type of the predicate.

        :returns Type: supertrype of the predicate.
        """
        return self.pred.super_type

    @property
    def value(self):
        """
        TODO
        """
        return self.pred.name

    def __str__(self):
        """
        Magic method to return this object in string form.
        """
        return '{}({})'.format(idp_name(self.pred.name),
                               ', '.join([arg.__str__() for arg in self.args]))
